# Adonis Webpack

This is a demo repo/Webpack starter-kit based off the [fullstack boilerplate](https://github.com/adonisjs/adonis-fullstack-app) for AdonisJs. The Webpack setup comes pre-configured with:

1. DEV: Hot Module Replacement (auto-injection of SASS/JavaScript changes)
2. DEV: Automatic page refreshes on template changes
3. PROD: Automatic `public` directory cleanup
4. PROD: Image/SVG Minification
5. PROD: JavaScript Uglification/Minification
6. PROD: SASS->CSS + CSS extraction into separate file
7. PROD: Asset hashing for better caching/invalidation
8. PROD: Automatic injection of asset hash filenames

## Development
Webpack is setup to run the Adonis server through a proxy so you will need to add the following two lines to your local .env file:
```
WEBPACK_HOST=localhost
WEBPACK_PORT=4444
```

Once this is done, you may start the Adonis server with `npm run server` and then in a new terminal window you can start Webpack with `npm run webpack`. 

All of the assets for the frontend can be found in the various directories within `resources`. You can make modifications to your SASS/JS at will and you will see them automatically show up. For example, once Webpack is running, open your browser's console and then modify the text in the `console.log` in `resources/js/app.js` or comment out the background image or color in `resources/sass/style.scss`. 

In addition, I've setup Webpack to watch all sub-directories within the `views` folder and automatically refresh the page anytime there is a change to one of the template files. You can test this out by editing/saving the `views/partials/tagline.edge` template file.  

## Production
In order to prepare all of your files and get them ready for production, you'll need to run the build script with `npm run build`. This will do a few things:
1. Clean out all files in the `public` directory. You can specify files/folders to preserve on line 89 of `webpack.config.js/env.prod.js`. Also, the process of deleting/re-creating files won't force you to re-commit files that haven't changed.
2. Minify your compiled CSS and extract it to it's own hashed file in `public`.
3. Minify and copy all your images/svgs to the `public` directory.
4. Uglify/Minify your JavaScript and split it appropriately into files in the `public` directory.
5. Generate a new `webpack_manifest.js` file to the `config` directory. This is used to inject the new hashed asset names for your JS/CSS into the global context via the `before.httpServer` hook (see `start/hooks.js`)

You will need to restart the server in order for any changes in your production files to be picked up.

### An important note on images
If you use a file url like the `resources/sass/style.scss` does currently:
```
background-image: url("../img/splash.png");
```
This will create a hashed image file name which will work properly from the compiled CSS file, but will be impossible to set properly through your templates. The image minification process will also re-run itself on these files in the `img` directory meaning your repo will start bloating. It's recommended, if you're using a lot of images/svgs in your SASS, that you have a directory for these inside of your SASS directory and leave the main `img` and `svg` folders for files being used in your templates.
