require('dotenv').config()
const merge = require('webpack-merge')

/**
 * Webpack Config
 * @param env
 * @returns {Promise<any>}
 */
module.exports = env => {
  return new Promise((resolve, reject) => {
    const common = require('./common')
    const envCfg = (env.dev === true)
                   ? require('./env.dev')
                   : require('./env.prod')

    envCfg
      .then(cfg => {
        resolve(merge(common, cfg))
      })
      .catch(err => reject(err))
  })
}
